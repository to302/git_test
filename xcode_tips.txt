== 한글로 된 데이타, 로그 출력 시 잘 보이게 <Project-name>-Prefix.pch 에 추가 ===
#define HanLog(s, ...) NSLog(@"%@", [NSString stringWithCString:[[NSString stringWithFormat:(s), ##__VA_ARGS__] cStringUsingEncoding:NSASCIIStringEncoding] encoding:NSNonLossyASCIIStringEncoding]);
